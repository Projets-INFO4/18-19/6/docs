# FICHA_info4_2018_2019


## January, 28th
* Discovery of the project
* Requirements: 
> 1.  User requirements: 
> * ease of connection to the app
> * history of thrown wastes
> * statistics of thrown wastes
> * rewards diversity and choice
> * user profile with history
> * goals to attein and achieve to motivate students to recycle
> 2. Merchants requirements:
> * vouchers in order to redynamize local trade
> 3. Non functional requirements :
> * Disponibility: weekly use of the application per user
> * Scaling: many simultaneous connections
> * Security: protected personal data
> * Usability: easy of use and intuitive app, connection via QR code
> * Portability: availability of the app on many platforms (PlayStore, AppStore)
> * Speed: reactivity, rapidity of using
* Drawing of the app's different parts
* Starting to design the app's different parts (on *MockFlow*)
* QR code research: https://www.npmjs.com/package/cordova-plugin-qrcodejs
* Brainstorm ideas:
> * connection with digital print
> * profile’s image
> * adding friends to profile
> * ranking with friends/world
> * monthly rewards
> * connection with pseudo or email or social networks
> * using ipad device

## February, 4th
* Meeting Monday 4th of February, 2:00 pm 248 room with Sylvain TORU and three GEM students
* Installing Microsoft Teams for the following and reporting with the other teams
> vincent hipault: vincent.hipault@grenoble-em.com - 0636946907

> hubert menard: hubert.menard@grenoble-em.com - 0673432964

> guillaume foucher: guillaume.foucher@grenoble-em.com - 0679817793

* Discovery of the graphic chart
* Discovery of Cliiink for inspiration
* **TODO**: Installing JHipster and initialize the application

## February, 11th
* Installing and running JHipster/Ionic
* `npm install -g generator-jhipster` > `npm install -g yo` > in a terminal: `./gradlew` > in another terminal `./npm start`
* how to create entities: `jhipster entity Foo`
* how to create the ionic App from the jhipster app: `ionic start fichapp oktadeveloper/jhipster`
* **TODO**: To resolve the error when login in the Ionic Jhipster app
* **TODO**: SCRUM To Do, In Progress, Done


## February, 18th
* Finishing to design the app's different parts on *MockFlow* and uploading on gitlab docs
* Scrum:

> **Stories:**
> * As a user, I want to sign in on the app.
> * As a user, I want to log in on the app.
> * As a user, I want to see my points. (Home Page)
> * As a user, I want to identify myself on a dumpster. (QR Code)
> * As a user, I want to see my statistics. (Statistics)
> * As a user, I want to see where are the dumpsters. (Map)
> * As a user, I want to trade my points into offers. (Offers)
> * As a user, I want to modify my informations (Options)
> * As a user, I want to log out. (Options)

> **To Do:**

> __Loading Page__
> - the logo with only "FI" changing of colors

> __Profile Page__
> - show user’s informations (possibility to modify)
> - \+ user can see his favorite offers
> - \+ informations about Ficha
> - add form to add new rewards if the user is a market

> __Offers Page__
> - /!\ Need infos from the product owners /!\
> - \+ choose favorite offers
> - \+\+ adapt offers according to his informations

> **In Progress:**

> **Done:**

/!\ NO NEED /!\
> __Map Page__
> - show locations of dumpsters on a map 
> - \+ get the user’s location and show him dumpsters next to him

> __Connection Page__
> - to connect
> - redirection to inscription

> __Home Page__
> - show points
> - redirect to other pages

> __Statistics Page__
> - show proportions of waste (Paper, Plastics, Metal, …)
> - /!\ Need infos from the dumpster /!\

> __QR Code Page__
> - show a unique QR Code to identify the user on the dumpster

> __Inscription Page__
> - fill informations
> - send a confirmation mail

> __Offers Page__
> - show different offers the user can get (or everything)

> __Profile Page__
> - show user’s informations
> - log out

## February, 19th
* Working on JHipster installation to fix bugs
* Creation of a ionic app `ionic-app` repository, to start working on the app without the Jhipster server

## March, 4th
* Designing the Home Page
* Planning for next days
* Meeting with the team
* QR code generation Page
* Circular Std font: trying to add it to our ionic app (custom font) -> FAIL
* JHipster installation finally fix

## March, 5th
* Working on the understanding of JHipster to handle users
    * Successfully created the entity client
    * Trying to create a ROLE_CLIENT /!\ not working yet
    * When we register on the app, it creates a new user in the webapp with a ROLE_USER
* Offers / Rewards Page

## March, 11th
* Meeting with Didier Donsez who explained to us how the database in JHipster works
    * Successfully changed the registration pages of the ionic application to have a username /!\ need to throw a new error if the login is already used /!\
    * Successfully created the ROLE_CLIENT
    * Successfully created the ROLE_COMMERCANT
    * Need to add a button in the registration page to indicate whether you will create a CLIENT account or a COMMERCANT account
* Meeting with the group : need to think about how the rewards will be materialize (Ideally the simpliest way possible)
* Need to create a COMMERCE entity in order to be easy for the COMMERCANT users to add their own COMMERCE to the app for future rewards
* Starting to merge the ionic app we developped alongside with the JHipster app => quite working, need some adjustements

## March, 12th
* Successfully merged the ionic app with the JHipster app
* Successfully created the button to indicate whether you are a CLIENT or a COMMERCANT
* Successfully created the COMMERCE entity
* Starting to work on how to improve the access-control of our JHipster app with those new ROLES we created

## March, 18th
* Mid-project meeting
* Reworked all the presentation of the page login, register, home
* Found a way to allow only a specific ROLE to create a new COMMERCE
* Replaced the map page with the user page where the user can modify his information

## March, 19th
* Profil page displaying to modify information about the user and adding a checkbox in the signup page
* When a new user register on the ionic app, he has the ROLE_USER and CLIENT, but for now we can't do nothing with that
* Trying to add a field commercant to the USER and USER DTO in order to modify which role a new user will get when he register

## March, 25th
* Trying to create new tables for client and modify them
* Trying to export local database
* Successfully created an UserExtra entity that extends User with a one-to-one relationship and thus add a field point to each user
* Starting to merge the different features we successfully implemented the past few weeks (new ROLES, new entities, ...)

## April, 1st
* Writing the report
* Meeting with Guillaume FOUCHER
* Meeting with Sylvain TORU
* spliting the rewards into categories for the Reward Page List
* trying to display on the map the locations of the rewards from the database instead of locally -> NOT WORKING
* blocking the change of points for users, only access to admin
* merging with master -> FINALE VERSION FINISHED
* updating the profile page to display the information about the user (no possible modification)